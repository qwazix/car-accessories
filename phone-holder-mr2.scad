l = 140;
t = 3;
w = 40;
h = 30;
r = 2;   // rounded corner radius
hl = 10; // lip height. With the bezelless phones of today
         // this shouldn't be too high but don't make it too
         // short either because it will affect stability
p = 10;  // phone thickness 

/* I suggest you choose a generous phone thickness and then 
fill the space with a foamy material. Thus you will make it
compatible with more devices */

rcube([l, w, t]);
translate([0, p+t, t-r]) rotate([-20,0,0]) rcube([l,t,h]);
translate([0, 0, t-r]) rotate([-20,0,0]) rcube([l,t,hl]);

module rcube(size){
    minkowski(){
        cube(size - [r,r,r]);
        sphere(d=r, $fn=100);
    }
}